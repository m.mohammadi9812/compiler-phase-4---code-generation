package main.visitor;

import main.ast.node.Main;
import main.ast.node.Program;
import main.ast.node.declaration.ActorDeclaration;
import main.ast.node.declaration.ActorInstantiation;
import main.ast.node.declaration.VarDeclaration;
import main.ast.node.declaration.handler.HandlerDeclaration;
import main.ast.node.declaration.handler.InitHandlerDeclaration;
import main.ast.node.expression.*;
import main.ast.node.expression.operators.BinaryOperator;
import main.ast.node.expression.operators.UnaryOperator;
import main.ast.node.expression.values.BooleanValue;
import main.ast.node.expression.values.IntValue;
import main.ast.node.expression.values.StringValue;
import main.ast.node.statement.*;
import main.ast.type.Type;
import main.ast.type.arrayType.ArrayType;
import main.ast.type.primitiveType.*;
import main.symbolTable.*;
import main.symbolTable.itemException.ItemNotFoundException;
import main.symbolTable.symbolTableVariableItem.SymbolTableHandlerArgumentItem;
import main.symbolTable.symbolTableVariableItem.SymbolTableLocalVariableItem;
import main.symbolTable.symbolTableVariableItem.SymbolTableVariableItem;

import javax.swing.plaf.nimbus.State;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

public class CodeGenerator implements Visitor{

    private PrintWriter actorpw, handlerpw, currpw;
    private String outDirPath =
            "C:\\Users\\Mohammad\\IdeaProjects\\compiler-phase-4---code-generation\\src\\main\\visitor\\gencode\\";
    private boolean inClass = true;
    private String currentActorName;
    private HandlerDeclaration handlerDeclaration;
    private int labelGenerator = 0;
    private int exitLoopLabel = -1, firstLoopLabel = -1;
    private int actorInstantiationIndex = -1;
    private boolean initFunction = false;

    private String getJVMTypeVar(Type type) {
        if (type instanceof IntType)
            return "I";
        else if (type instanceof BooleanType)
            return "Z";
        else if (type instanceof ArrayType)
            return "[I";
        else if (type instanceof StringType)
            return "Ljava/lang/String";
        else {
            return "L" + type.toString();
        }
//        return (type instanceof IntType) ? "I" :
//                (type instanceof BooleanType) ? "Z" :
//                        (type instanceof ArrayType) ? "[I" :
//                        (type instanceof StringType) ? "Ljava/lang/String" :
//                                 DONE: it probably should change !!
//                                "L" + type.toString();
    }

    private Type getVariableType(String name) {
        SymbolTable actorSymTab = null;
        try {
            String handlerName = this.handlerDeclaration.getName().getName();
            SymbolTableActorItem actorItem = (SymbolTableActorItem)SymbolTable.root.get(SymbolTableActorItem.STARTKEY + this.currentActorName);
            actorSymTab = actorItem.getActorSymbolTable();
            SymbolTableHandlerItem handlerItem = (SymbolTableHandlerItem) actorSymTab.get(SymbolTableHandlerItem.STARTKEY + handlerName);
            SymbolTable handlerSymTab = handlerItem.getHandlerSymbolTable();
            SymbolTableVariableItem item = (SymbolTableVariableItem) handlerSymTab.get(SymbolTableVariableItem.STARTKEY + name);
            return item.getType();
        } catch (ItemNotFoundException ignored) {
            SymbolTableVariableItem variableItem = null;
            try {
                variableItem = (SymbolTableVariableItem) actorSymTab.get(SymbolTableVariableItem.STARTKEY + name);
            } catch (ItemNotFoundException ignored2) {}
            return variableItem.getType();
        }
    }

//    private Type getVariableType(Identifier id) {
//        Type result = null;
//        ArrayList<VarDeclaration> argsVarDec = this.handlerDeclaration.getArgs(),
//                localsVarDec = this.handlerDeclaration.getLocalVars();
//        for (VarDeclaration arg : (argsVarDec != null) ? argsVarDec : new ArrayList<VarDeclaration>()) {
//            if (arg.getIdentifier().getName().equals(id.getName()))
//                result = arg.getType();
//        }
//        for (VarDeclaration local : (localsVarDec != null) ? localsVarDec : new ArrayList<VarDeclaration>()) {
//            if (local.getIdentifier().getName().equals(id.getName()))
//                result = local.getType();
//        }
//        return result;
//    }

    //DONE: implement Slot of
    private int slotOf(String identifier) {
        // DONE: locals are after args, needs to change
        ArrayList<VarDeclaration> argsVarDec = this.handlerDeclaration.getArgs(),
                localsVarDec = this.handlerDeclaration.getLocalVars();
        ArrayList<String> args = new ArrayList<>(), locals = new ArrayList<>();
        for (VarDeclaration arg : (argsVarDec != null) ? argsVarDec : new ArrayList<VarDeclaration>()) {
            args.add(arg.getIdentifier().getName());
        }
        for (VarDeclaration local : (localsVarDec != null) ? localsVarDec : new ArrayList<VarDeclaration>()) {
            locals.add(local.getIdentifier().getName());
        }
        int result = -1;
        if (args.contains(identifier))
            result = args.indexOf(identifier) + 2;
        else if(locals.contains(identifier))
            result = locals.indexOf(identifier) + args.size() + 1;
        return result;
    }

    private String actorVarAccess(String name, boolean loadThis, boolean getField) {
        String result = "";
        try {
            SymbolTableActorItem actorItem = (SymbolTableActorItem) SymbolTable.root.get(SymbolTableActorItem.STARTKEY + this.currentActorName);
            SymbolTableVariableItem variableItem = (SymbolTableVariableItem) actorItem.getActorSymbolTable().get(SymbolTableVariableItem.STARTKEY + name);
            Type varType = variableItem.getType();
            result += (loadThis ? "aload_0\n" : "");
            result += (getField ? "getfield" : "putfield") + " " + this.currentActorName + "/" + name + " " +
                    this.getJVMTypeVar(varType) + (isPrimitive(varType) ? "" : ";") + "\n";
        } catch (ItemNotFoundException ignored) {}
        return result;
    }

    private boolean isPrimitive(Type type) {
        return type instanceof IntType || type instanceof BooleanType;
    }

    private String getActorConstructor() {
        return ".method public <init>(I)V\n" + ".limit stack 2\n" + ".limit locals 2\n" +
                "aload_0\n" + "iload_1\n" + "invokespecial Actor/<init>(I)V\n" +
                "return\n" + ".end method\n";
    }

    private String getKnownActorsFunction(ArrayList<VarDeclaration> knownActors, String actorName) {
        // TODO: use this.currentActorName instead of argument
        StringBuilder setFunctionString = new StringBuilder();
        setFunctionString.append(".method public setKnownActors(");
        Set<String> knownActorsTypes = new HashSet<>();
        for (VarDeclaration knownActor : knownActors)
            knownActorsTypes.add(knownActor.getType().toString());
        for (String s : knownActorsTypes)
            setFunctionString.append("L").append(s).append(";");
        setFunctionString.append(")V\n");
        setFunctionString.append(".limit stack 16\n");
        setFunctionString.append(".limit locals ").append(knownActorsTypes.size() + 1).append("\n");
        int i = 0;
        for (VarDeclaration knownActor : knownActors) {
            setFunctionString.append("aload_0\n");
            setFunctionString.append("aload").append( (++i <= 3 ? "_" : " ") ).append(i).append("\n");
            // TODO: change it with actorVarAccess function
            setFunctionString.append("putfield ").append(actorName).append("/")
                                .append(knownActor.getIdentifier().getName()).append(" L")
                                .append(knownActor.getType().toString()).append(";\n");
        }
        setFunctionString.append("return\n").append(".end method");
        return setFunctionString.toString();
    }

    private String getHandlerSenderFunction(HandlerDeclaration handlerDeclaration) {
        // TODO: safe for loop
        String handlerName = handlerDeclaration.getName().getName();
        StringBuilder handlerSenderFunction = new StringBuilder();
        ArrayList<VarDeclaration> handlerArgs = handlerDeclaration.getArgs();
        handlerSenderFunction.append(".method public send_").append(handlerName)
                                    .append("(LActor;");
        for (int j = 0; j < handlerArgs.size(); j++ ) {
            Type handlerArgType = handlerArgs.get(j).getType();
            handlerSenderFunction.append(this.getJVMTypeVar(handlerArgType))
                    .append((j != handlerArgs.size() - 1) ? ";" : "");
        }
        handlerSenderFunction.append(")V\n");
        // DONE: .limit stack and .limit locals
        handlerSenderFunction.append(".limit stack 16\n.limit locals 16\n");
        handlerSenderFunction.append("aload_0\n").append("new ").append(currentActorName).append("_")
                .append(handlerName).append("\n");
        handlerSenderFunction.append("dup\n").append("aload_0\n").append("aload_1\n");
        int i = 1;
        for (VarDeclaration arg : handlerArgs) {
            handlerSenderFunction.append((arg.getType() instanceof IntType) ? "i" : "a")
                    .append("load").append((++i <= 3) ? "_" : " ").append(i).append("\n");
        }
        handlerSenderFunction.append("invokespecial ").append(currentActorName).append("_").append(handlerName)
                .append("/<init>(L").append(currentActorName).append(";LActor;");
        for (i = 0; i < handlerArgs.size(); i++) {
            Type handlerArgType = handlerArgs.get(i).getType();
            handlerSenderFunction.append(this.getJVMTypeVar(handlerArgType))
                                        .append((i != handlerArgs.size() - 1) ? ";" : "");
        }
        handlerSenderFunction.append(")").append("V\n").append("invokevirtual ").append(currentActorName)
                            .append("/send(LMessage;)V\n");
        handlerSenderFunction.append("return\n").append(".end method\n");
        return handlerSenderFunction.toString();
    }

    private int getQueueSize(String actorName) {
        int result = -1;
        try {
            SymbolTableMainItem mainItem = (SymbolTableMainItem) SymbolTable.root.get(SymbolTableMainItem.STARTKEY + "main");
            SymbolTable mainSymTab = mainItem.getMainSymbolTable();
            SymbolTableVariableItem mainVariable = (SymbolTableVariableItem) mainSymTab.get(SymbolTableVariableItem.STARTKEY + actorName);
            Type mainVariableType = mainVariable.getType();
            SymbolTableActorItem actorItem = (SymbolTableActorItem) mainSymTab.get(SymbolTableActorItem.STARTKEY + mainVariableType.toString());
            ActorDeclaration actorDeclaration = actorItem.getActorDeclaration();
            result = actorDeclaration.getQueueSize();
        } catch (ItemNotFoundException ignored) {}
        return result;
    }

    private String handlerFile() {
        String result = "";
        String handlerName = this.handlerDeclaration.getName().getName();
        String handlerFileName = this.currentActorName + "_" + handlerName;
        result += ".class public " + handlerFileName + "\n.super Message\n\n\n";
        ArrayList<VarDeclaration> handlerArgs = (this.handlerDeclaration.getArgs() != null ? this.handlerDeclaration.getArgs() : new ArrayList<>());
        for (VarDeclaration handlerArg : handlerArgs)
            result += ".field private " + handlerArg.getIdentifier().getName() + " " + getJVMTypeVar(handlerArg.getType()) + "\n";
        result += ".field private receiver L" + this.currentActorName + ";\n.field private sender LActor;\n\n";
        result += ".method public <init>(L" + this.currentActorName + ";LActor;";
        for (VarDeclaration handlerArg : handlerArgs)
            result += getJVMTypeVar(handlerArg.getType());
        result += ")V\n.limit stack 16\n.limit locals 16\n";
        result += "aload_0\n" +
                "invokespecial Message/<init>()V\n" +
                "aload_0\n" +
                "aload_1\n" +
                "putfield " + handlerFileName + "/receiver L" + this.currentActorName + ";\n" +
                "aload_0\n" +
                "aload_2\n" +
                "putfield " + handlerFileName + "/sender LActor;\n";
        for (int i = 0; i < handlerArgs.size(); i++) {
            Type handlerType = handlerArgs.get(i).getType();
            result += "aload_0\n" +
                    (this.isPrimitive(handlerType) ? "i" : "a") + "load" + (i < 1 ? "_" : " ") + (i+3) + "\n" +
                    "putfield " + handlerFileName + "/" + handlerArgs.get(i).getIdentifier().getName() + " " + getJVMTypeVar(handlerType) + "\n";
        }
        result += "return\n" +
                ".end method\n" +
                "\n" +
                ".method public execute()V\n" +
                ".limit stack 16\n" +
                ".limit locals 16\n";
        result += "aload_0\n" +
                "getfield " + handlerFileName + "/receiver L" + this.currentActorName + ";\n" +
                "aload_0\n" +
                "getfield " + handlerFileName + "/sender LActor;\n";
        for (VarDeclaration handlerArg : handlerArgs) {
            result += "aload_0\n";
            result += "getfield " + handlerFileName + "/" + handlerArg.getIdentifier().getName()
                    + " "  + getJVMTypeVar(handlerArg.getType()) + (isPrimitive(handlerArg.getType()) ? "" : ";");
        }
        result += "\n";
        result += "invokevirtual " + this.currentActorName +"/" + handlerName + "(LActor;";
        for (VarDeclaration handlerArg : handlerArgs)
            result += getJVMTypeVar(handlerArg.getType());
        result += ")V\n";
        result += "return\n" +
                ".end method\n";
        return result;
    }

    private String boolCMPOperator(String op) {
        String result = "";
        result += "if_icmp" + op + " Label" + labelGenerator + "\n";
        result += "ldc 1\n";
        result += "goto Label" + (labelGenerator + 1) + "\n";
        result += "Label" + (labelGenerator++) + ":\n";
        result += "ldc 0\n";
        result += "Label" + (labelGenerator++) + ":\n";
        return result;
    }

    private String getSignature(String actorName, String msgHandlerName) {
        String result = "";
        try {
            SymbolTableActorItem actorItem = (SymbolTableActorItem) SymbolTable.root.get(SymbolTableActorItem.STARTKEY + actorName);
            SymbolTable actorSymbolTable = actorItem.getActorSymbolTable();
            SymbolTableHandlerItem handlerItem = (SymbolTableHandlerItem) actorSymbolTable.get(SymbolTableHandlerItem.STARTKEY + msgHandlerName);
//            SymbolTable handlerSymbolTable = handlerItem.getHandlerSymbolTable();
            HandlerDeclaration handlerDeclaration = handlerItem.getHandlerDeclaration();
//            result += "(";
            for (VarDeclaration handlerArg : handlerDeclaration.getArgs())
                result += getJVMTypeVar(handlerArg.getType());
            result += ")V";
            return result;
        } catch (ItemNotFoundException ignored) {}
        return result;
    }

    private Type getKnownActorFromName(String variableName) {
        Type result = null;
        try {
            SymbolTableActorItem actorItem = (SymbolTableActorItem) SymbolTable.root.get(SymbolTableActorItem.STARTKEY + this.currentActorName);
            ActorDeclaration actorDeclaration = actorItem.getActorDeclaration();
            ArrayList<VarDeclaration> knownActors = (actorDeclaration.getKnownActors() != null ? actorDeclaration.getKnownActors() : new ArrayList<>());
            for (VarDeclaration knownActor : knownActors)
                if (knownActor.getIdentifier().getName().equals(variableName))
                    result = knownActor.getType();
        } catch (ItemNotFoundException ignored) {}
        return result;
    }

    private Type getExpressionType(Expression expression) {
        Type result = null;
        if (expression instanceof BinaryExpression) {
            BinaryOperator binaryOperator = ((BinaryExpression) expression).getBinaryOperator();
            switch (binaryOperator) {
                case add:case sub:case div:case mod:case mult:
                    result = new IntType();
                    break;
                case eq:case neq:case gt:case lt:case or:case and:
                    result = new BooleanType();
                    break;
            }
        } else if (expression instanceof UnaryExpression) {
            UnaryOperator unaryOperator = ((UnaryExpression) expression).getUnaryOperator();
            switch (unaryOperator) {
                case postinc:case preinc:case predec:case postdec:case minus:
                    result = new IntType();
                    break;
                case not:
                    result = new BooleanType();
                    break;
            }
        }
        return result;
    }

//    private String callSetKnownActorsFunction(String actorName) {
//
//    }

    protected void visitStatement( Statement stat )
    {
        if( stat == null )
            return;
        else if( stat instanceof MsgHandlerCall)
            this.visit( ( MsgHandlerCall ) stat );
        else if( stat instanceof Block)
            this.visit( ( Block ) stat );
        else if( stat instanceof Conditional)
            this.visit( ( Conditional ) stat );
        else if( stat instanceof For)
            this.visit( ( For ) stat );
        else if( stat instanceof Break )
            this.visit( ( Break ) stat );
        else if( stat instanceof Continue )
            this.visit( ( Continue ) stat );
        else if( stat instanceof Print )
            this.visit( ( Print ) stat );
        else if( stat instanceof Assign )
            this.visit( ( Assign ) stat );
    }

    protected void visitExpr( Expression expr )
    {
        if( expr == null )
            return;
        else if( expr instanceof UnaryExpression)
            this.visit( ( UnaryExpression ) expr );
        else if( expr instanceof BinaryExpression)
            this.visit( ( BinaryExpression ) expr );
        else if( expr instanceof ArrayCall)
            this.visit( ( ArrayCall ) expr );
        else if( expr instanceof ActorVarAccess)
            this.visit( ( ActorVarAccess ) expr );
        else if( expr instanceof Identifier )
            this.visit( ( Identifier ) expr );
        else if( expr instanceof Self )
            this.visit( ( Self ) expr );
        else if( expr instanceof Sender )
            this.visit( ( Sender ) expr );
        else if( expr instanceof BooleanValue)
            this.visit( ( BooleanValue ) expr );
        else if( expr instanceof IntValue)
            this.visit( ( IntValue ) expr );
        else if( expr instanceof StringValue)
            this.visit( ( StringValue ) expr );
    }

    @Override
    public void visit(Program program) {
        for (ActorDeclaration actor : program.getActors()) {
            this.visit(actor);
        }
        this.visit(program.getMain());
    }

    @Override
    public void visit(ActorDeclaration actorDeclaration) {
        String actorName = actorDeclaration.getName().getName();
        this.currentActorName = actorName;
        try {
            File file = new File(this.outDirPath + actorName + ".j");
            if (!file.exists())
                file.createNewFile();
            FileWriter fw = new FileWriter(file, false);
            this.actorpw = new PrintWriter(fw);
            actorpw.println(".class public " + actorName);
            actorpw.println(".super Actor");
            actorpw.println();
            this.inClass = true;
            for (VarDeclaration knownActor : actorDeclaration.getKnownActors())
                this.visit(knownActor);
            for (VarDeclaration actorVar : actorDeclaration.getActorVars())
                this.visit(actorVar);
            this.inClass = false;
            this.currpw = actorpw;
            actorpw.println();
            actorpw.println(this.getActorConstructor());
            /*---------------------- visit init function ------------------------------------*/
            initFunction = true;
            if (actorDeclaration.getInitHandler() != null)
                this.visit(actorDeclaration.getInitHandler());
            initFunction = false;
            /*---------------------- end of visit init function ------------------------------------*/
            actorpw.println(this.getKnownActorsFunction(actorDeclaration.getKnownActors(), actorName));
            actorpw.println();
            for (HandlerDeclaration handler : actorDeclaration.getMsgHandlers())
                this.visit(handler);
        } catch (IOException ignored) {}
        finally {
            if (this.actorpw != null) {
                this.actorpw.close();
            }
        }
    }

    @Override
    public void visit(HandlerDeclaration handlerDeclaration) {
        this.handlerDeclaration = handlerDeclaration;
        if(!initFunction)
            actorpw.println(this.getHandlerSenderFunction(handlerDeclaration));
        /*--------------------------- generate function declaration part -------------------------*/
        String handlerName = handlerDeclaration.getName().getName();
        actorpw.print(".method public " + handlerName + (initFunction ? "(" : "(LActor;"));
        ArrayList<VarDeclaration> handlerArgs = handlerDeclaration.getArgs();
        for (VarDeclaration argsDecl : (handlerArgs != null ? handlerArgs : new ArrayList<VarDeclaration>())) {
            Type argumentDeclType = argsDecl.getType();
            actorpw.print(getJVMTypeVar(argumentDeclType));
        }
        actorpw.println(")V");
        actorpw.println(".limit stack 16\n.limit locals 16");
        for (Statement handlerStatement : handlerDeclaration.getBody()) {
            this.visitStatement(handlerStatement);
        }
        actorpw.println("return\n.end method\n");
        /*------------------------------end of generating function in actor file part ------------------------------*/
        if(initFunction)
            return;
        // -____-
        /*----------------------------- new file for handler ------------------------------------------------*/
        File file = new File(this.outDirPath + currentActorName + "_" + handlerName + ".j");
        FileWriter fw = null;
        try {
            if (!file.exists())
                file.createNewFile();
            fw = new FileWriter(file, false);
            this.handlerpw = new PrintWriter(fw);
            this.currpw = this.handlerpw;
            String handlerFileContent = handlerFile();
            handlerpw.print(handlerFileContent);
        } catch (IOException ignored) {
            System.err.println("WOW!!!");
        } finally {
            this.handlerpw.close();
            this.currpw = this.actorpw;
        }
    }

    @Override
    public void visit(VarDeclaration varDeclaration) {
        String varName = varDeclaration.getIdentifier().getName();
        Type varType = varDeclaration.getType();
        String varTypeString = getJVMTypeVar(varType);
        if (this.inClass) {
            String instruction = ".field " + varName + " " + varTypeString + ( (this.isPrimitive(varType)) ? "" : ";");
            actorpw.println(instruction);
        }
    }

    @Override
    public void visit(Main mainActors) {
        File file = new File(this.outDirPath + "Main.j");
        FileWriter fw = null;
        try {
            if (!file.exists())
               file.createNewFile();
            fw = new FileWriter(file, false);
            PrintWriter mainWriter = this.currpw = new PrintWriter(fw);
            currpw.println(".class public Main\n.super java/lang/Object\n\n\n.method public <init>()V");
            currpw.println(".limit stack 16\n.limit locals 16\naload_0\ninvokespecial java/lang/Object/<init>()V\nreturn\n.end method\n");
            currpw.println(".method public static main([Ljava/lang/String;)V\n" +
                    ".limit stack 16\n" +
                    ".limit locals 16");
            int i = 1;
            ArrayList<String> slots = new ArrayList<>();
            ArrayList<String> types = new ArrayList<>();
            slots.add(0, "this");
            types.add(0, "this");

            for(ActorInstantiation actorIns: mainActors.getMainActors()){
                this.actorInstantiationIndex = i;
                slots.add(i, actorIns.getIdentifier().getName());
                types.add(i, actorIns.getType().toString());
                this.visit(actorIns);
                i++;
            }

            String type= "";
            for(ActorInstantiation actorIns: mainActors.getMainActors()){
                currpw.println("aload " + slots.indexOf(actorIns.getIdentifier().getName()));
                type= "";
                for (Identifier knownActor: actorIns.getKnownActors()){
                    int slotOfKA = slots.indexOf(knownActor.getName());
                    currpw.println("aload " + slotOfKA);
                    type += "L" + types.get(slotOfKA) + ";";
                }
                currpw.print("invokevirtual " + actorIns.getType().toString() + "/setKnownActors(");
                currpw.println( type + ")V");
            }

            for(ActorInstantiation actorIns: mainActors.getMainActors()){
                SymbolTableActorItem actorItem = null;
                try {
                    actorItem = (SymbolTableActorItem) SymbolTable.root.get(SymbolTableActorItem.STARTKEY + actorIns.getType().toString());
                    InitHandlerDeclaration initHandler = actorItem.getActorDeclaration().getInitHandler();
                    if(initHandler != null){
                        int slotOfAI = slots.indexOf(actorIns.getIdentifier().getName());
                        currpw.println("aload " + slotOfAI);
                        for(Expression initArg: actorIns.getInitArgs()){
                            visitExpr(initArg);
                        }
                        currpw.println("invokevirtual " + actorIns.getType().toString() +"/initial()V");
                    }
                } catch (ItemNotFoundException ignored) {}
            }

            for(ActorInstantiation actorIns: mainActors.getMainActors()) {
                int slotOfAI = slots.indexOf(actorIns.getIdentifier().getName());
                currpw.println("aload " + slotOfAI);
                currpw.println("invokevirtual " + types.get(slotOfAI) + "/start()V");
            }

            this.currpw = (mainWriter);
            currpw.println("return\n" +
                    ".end method");
        } catch (IOException ignored) {}
        finally {
            try {
                assert fw != null;
                fw.close();
            } catch (IOException ignored) {}
        }
    }

    @Override
    public void visit(ActorInstantiation actorInstantiation) {
        String actorName = actorInstantiation.getIdentifier().getName();
        currpw.println("new " + actorInstantiation.getType().toString() + "\ndup");
        currpw.println("ldc " + getQueueSize(actorName));
        currpw.println("invokespecial " + actorInstantiation.getType().toString() + "/<init>(I)V");
        currpw.println("astore" + (this.actorInstantiationIndex > 4 ? " " : "_") + actorInstantiationIndex);
    }


    @Override
    public void visit(UnaryExpression unaryExpression) {
        UnaryOperator operator = unaryExpression.getUnaryOperator();
        Expression operand = unaryExpression.getOperand();
        switch (operator) {
            case postinc: case preinc:
                BinaryExpression binaryExpression = new BinaryExpression(operand, new IntValue(1, new IntType()), BinaryOperator.add);
                binaryExpression.setType(new IntType());
                Assign binaryAssign = new Assign(operand, binaryExpression);
                if (operator == UnaryOperator.preinc) {
                    this.visit(binaryAssign);
                    this.visitExpr(operand);
                } else {
                    this.visitExpr(operand);
                    this.visit(binaryAssign);
                }
                break;
            case predec: case postdec:
                BinaryExpression binaryExpression1 = new BinaryExpression(operand, new IntValue(1, new IntType()), BinaryOperator.sub);
                binaryExpression1.setType(new IntType());
                Assign binaryAssign1 = new Assign(operand, binaryExpression1);
                if (operator == UnaryOperator.predec) {
                    this.visit(binaryAssign1);
                    this.visitExpr(operand);
                } else {
                    this.visitExpr(operand);
                    this.visit(binaryAssign1);
                }
                break;
            case not:
                this.visitExpr(operand);
                currpw.println("ifne Label" + labelGenerator);
                currpw.println("ldc 1");
                currpw.println("goto Label" + (labelGenerator + 1) );
                currpw.println("Label" + (labelGenerator++) + ":");
                currpw.println("ldc 0");
                currpw.println("Label" + (labelGenerator++) + ":");
                break;
            case minus:
                this.visitExpr(operand);
                currpw.println("ineg");
                break;
        }
    }

    @Override
    public void visit(BinaryExpression binaryExpression) {
        BinaryOperator operator = binaryExpression.getBinaryOperator();
        switch (operator) {
            case add:
                this.visitExpr(binaryExpression.getLeft());
                this.visitExpr(binaryExpression.getRight());
                currpw.println("iadd");
                break;
            case sub:
                this.visitExpr(binaryExpression.getLeft());
                this.visitExpr(binaryExpression.getRight());
                currpw.println("isub");
                break;
            case mult:
                this.visitExpr(binaryExpression.getLeft());
                this.visitExpr(binaryExpression.getRight());
                currpw.println("imul");
                break;
            case div:
                this.visitExpr(binaryExpression.getLeft());
                this.visitExpr(binaryExpression.getRight());
                currpw.println("idiv");
                break;
            case mod:
                this.visitExpr(binaryExpression.getLeft());
                this.visitExpr(binaryExpression.getRight());
                currpw.println("irem");
                break;
            case eq:
                this.visitExpr(binaryExpression.getLeft());
                this.visitExpr(binaryExpression.getRight());
                currpw.println(boolCMPOperator("ne"));
                break;
            case neq:
                this.visitExpr(binaryExpression.getLeft());
                this.visitExpr(binaryExpression.getRight());
                currpw.println(boolCMPOperator("eq"));
                break;
            case gt:
                this.visitExpr(binaryExpression.getLeft());
                this.visitExpr(binaryExpression.getRight());
                currpw.println(boolCMPOperator("le"));
                break;
            case lt:
                this.visitExpr(binaryExpression.getLeft());
                this.visitExpr(binaryExpression.getRight());
                currpw.println(boolCMPOperator("ge"));
                break;
            case or:
                this.visitExpr(binaryExpression.getLeft());
                currpw.println("ifne Label" + labelGenerator);
                this.visitExpr(binaryExpression.getRight());
                currpw.println("ifeq Label" + (labelGenerator + 1));
                currpw.println("Label" + labelGenerator + ":");
                currpw.println("ldc 1");
                currpw.println("goto Label" + (labelGenerator + 2));
                currpw.println("Label" + (labelGenerator + 1) + ":");
                currpw.println("ldc 0");
                currpw.println("Label" + (labelGenerator + 2) + ":");
                labelGenerator += 3;
                break;
            case and:
                this.visitExpr(binaryExpression.getLeft());
                currpw.println("ifeq Label" + labelGenerator);
                this.visitExpr(binaryExpression.getRight());
                currpw.println("ifeq Label" + labelGenerator);
                currpw.println("ldc 1");
                currpw.println("goto Label" + (labelGenerator + 1));
                currpw.println("Label" + (labelGenerator++) + ":");
                currpw.println("ldc 0");
                currpw.println("Label" + (labelGenerator++) + ":");
                break;
            case assign:
                Assign binaryAssign = new Assign(binaryExpression.getLeft(), binaryExpression.getRight());
                this.visit(binaryAssign);
                this.visitExpr(binaryExpression.getLeft());
                break;
        }
    }

    @Override
    public void visit(ArrayCall arrayCall) {
        Identifier arrayId;
        if (arrayCall.getArrayInstance() instanceof ActorVarAccess)
            arrayId = ((ActorVarAccess)arrayCall.getArrayInstance()).getVariable();
        else arrayId = (Identifier)arrayCall.getArrayInstance();
        this.visit(arrayId);
        this.visitExpr(arrayCall.getIndex());
    }

    @Override
    public void visit(ActorVarAccess actorVarAccess) {
        this.visit(actorVarAccess.getVariable());
    }

    @Override
    public void visit(Identifier identifier) {
        int idIndex = slotOf(identifier.getName());
        Type idType = (identifier.getType() != null ? identifier.getType() : getVariableType(identifier.getName()));
        if (idIndex != -1)
            currpw.println((isPrimitive(idType) ? "i" : "a") + "load" + (idIndex >= 5 ? " " : "_") + idIndex);
        else {
            currpw.println(this.actorVarAccess(identifier.getName(), true, true));
        }
    }

    @Override
    public void visit(Self self) {

    }

    @Override
    public void visit(Sender sender) {

    }

    @Override
    public void visit(BooleanValue value) {
        String valueCode = "ldc " + ((value.getConstant()) ? "1" : "0") + "\n";
        currpw.print(valueCode);
    }

    @Override
    public void visit(IntValue value) {
        String valueCode = "ldc " + value.getConstant() + "\n";
        currpw.print(valueCode);
    }

    @Override
    public void visit(StringValue value) {
        String valueCode = "ldc " + value.getConstant() + "\n";
        currpw.print(valueCode);
    }

    @Override
    public void visit(Block block) {
        for (Statement blockStatement : block.getStatements())
            this.visitStatement(blockStatement);
    }

    @Override
    public void visit(Conditional conditional) {
        int localLabel = labelGenerator;
        labelGenerator += 2;
        this.visitExpr(conditional.getExpression());
        currpw.println("ifeq Label" + localLabel);
        if (conditional.getThenBody() != null)
            this.visitStatement(conditional.getThenBody());
        currpw.println("goto Label" + (localLabel + 1));
        currpw.println("Label" + localLabel + ":");
        if (conditional.getElseBody() != null)
            this.visitStatement(conditional.getElseBody());
        currpw.println("Label" + (localLabel + 1) + ":");
    }

    @Override
    public void visit(For loop) {
        this.visit(loop.getInitialize());
        firstLoopLabel = labelGenerator;
        exitLoopLabel = labelGenerator + 1;
        labelGenerator += 2;
        currpw.println("Label" + firstLoopLabel + ":");
        this.visitExpr(loop.getCondition());
        currpw.println("ifeq Label" + exitLoopLabel);
        this.visitStatement(loop.getBody());
        this.visit(loop.getUpdate());
    }

    @Override
    public void visit(Break breakLoop) {
        currpw.println("goto Label" + exitLoopLabel);
    }

    @Override
    public void visit(Continue continueLoop) {
        currpw.println("goto Label" + firstLoopLabel);
    }

    @Override
    public void visit(MsgHandlerCall msgHandlerCall) {
        Expression msgHandlerInstance = msgHandlerCall.getInstance();
        String signature = "";
        if (msgHandlerInstance == null || msgHandlerInstance instanceof Self) {
            currpw.println("aload_0");
        } else if( msgHandlerInstance instanceof Sender ) {
            currpw.println("aload_1");
        } else {
            Identifier msgInst = ((Identifier) msgHandlerInstance);
            currpw.println(this.actorVarAccess(msgInst.getName(), true, true));
        }
        currpw.println("aload_0");
        for (Expression msgHandlerArg: msgHandlerCall.getArgs())
            this.visitExpr(msgHandlerArg);
        String className = "";
        String msgHandlerName = msgHandlerCall.getMsgHandlerName().getName();
        if (msgHandlerInstance == null || msgHandlerInstance instanceof Self) {
            className = this.currentActorName;
            signature = "(LActor;" + getSignature(className, msgHandlerName);
        }
        else if (!(msgHandlerInstance instanceof Sender)) {
            SymbolTableActorItem currentActor = null;
            String instName = ((Identifier) msgHandlerInstance).getName();
            SymbolTableVariableItem actorSymVarTab = null;
//            try {
//                currentActor = (SymbolTableActorItem) SymbolTable.root.get(SymbolTableActorItem.STARTKEY +);
//            } catch (ItemNotFoundException ignored) {}
//            try {
//                String key = SymbolTableVariableItem.STARTKEY + instName;
//                actorSymVarTab = (SymbolTableVariableItem) currentActor.getActorSymbolTable().get(key);
//                className = actorSymVarTab.getType().toString();
//            } catch (ItemNotFoundException ignored) {
//                System.err.println("WOW2");
//            }
            className =  getKnownActorFromName(((Identifier) msgHandlerInstance).getName()).toString();
            String msgHandlerSignature = getSignature(className, msgHandlerName);
            signature = "(LActor;" + msgHandlerSignature;
        } else {
            signature += "(LActor;";
            for (Expression arg : msgHandlerCall.getArgs()) {
                if (arg instanceof Identifier) {
                    String argName = ((Identifier) arg).getName();
                    Type variableType = getVariableType(argName);
                    signature += getJVMTypeVar(variableType);
                } else {
                    Type expressionType = getExpressionType(arg);
                    signature += getJVMTypeVar(expressionType);
                }
            }
            signature += ")V";
        }
        currpw.print("invokevirtual ");
        if (msgHandlerInstance instanceof Sender)
            currpw.print("Actor");
        else
            currpw.print(className);
        currpw.print("/send_" + msgHandlerName);
//        for (Expression arg : msgHandlerCall.getArgs()) {
//            Type argumentType = arg.getType();
//            currpw.print(getJVMTypeVar(argumentType));
//        }
        currpw.println(signature);
//        currpw.print(")V");
    }

    @Override
    public void visit(Print print) {
        currpw.println("getstatic java/lang/System/out Ljava/io/PrintStream;\n");
        this.visitExpr(print.getArg());
        // DONE: needs change in signature
        Type printArgType = null;
        if (print.getArg().getType() != null)
            printArgType = print.getArg().getType();
        else {
            String argName = ((Identifier) print.getArg()).getName();
            printArgType = getVariableType(argName);
        }
        currpw.println("invokevirtual java/io/PrintStream/println(" + getJVMTypeVar(printArgType) + (isPrimitive(printArgType) ? "" : ";") + ")V\n");
    }

    @Override
    public void visit(Assign assign) {
        // DONE: needs HUGE changes!!!
        // DONE: check if 5 needs '_' or ' '
        Expression lvalue = assign.getlValue(); // get left value
        Type varType = (lvalue.getType() != null ? lvalue.getType() : getVariableType(((Identifier)lvalue).getName())); // left value type, used in isPrimitive function
        if (lvalue instanceof Identifier || (lvalue instanceof ActorVarAccess && !(lvalue.getType() instanceof ArrayType)) ) { // we need to put right value in left value
            if (lvalue instanceof ActorVarAccess)
                lvalue = ((ActorVarAccess)lvalue).getVariable();
            int varIndexInSlot = slotOf(((Identifier) lvalue).getName()); // get index of left value identifier
            String storeCode = "";
            if (varIndexInSlot != -1) { // if it's present in local vars and arguments ....
                this.visitExpr(assign.getrValue()); // visit right expression
                storeCode = (this.isPrimitive(varType) ? "i" : "a") + "store" + ((varIndexInSlot > 5) ? " " : "_")
                        + varIndexInSlot + "\n";
            } else {
                currpw.println("aload_0"); // load this ...
                this.visitExpr(assign.getrValue()); // visit right value expression
                String storeVarAccessCode = this.actorVarAccess(((Identifier) lvalue).getName(), false, false);
                storeCode += storeVarAccessCode; // putfield the variable
            }
            currpw.println(storeCode);
        } else if (lvalue instanceof ArrayCall) {
            // If lvalue is array call, we need to put array instance
            // below every thing, then put array index on top of that
            // and then put right value on top of them, and them run "iastore"
            Identifier arrayInstance = null;
            if (((ArrayCall) lvalue).getArrayInstance() instanceof ActorVarAccess) {
                arrayInstance = ((ActorVarAccess) ((ArrayCall) lvalue).getArrayInstance()).getVariable();
            } else arrayInstance = (Identifier) ((ArrayCall)lvalue).getArrayInstance();
            int varIndexInSlot = slotOf(arrayInstance.getName());
            if (varIndexInSlot != -1) { // if array is present in arguments
                currpw.println("aload" + (varIndexInSlot <= 5 ? "_" : " ") + varIndexInSlot + "\n"); // load array
            } else { // else getfield from class and load it to stack
                String getFieldArray = this.actorVarAccess(arrayInstance.getName(), true, true);
                this.currpw.println(getFieldArray);
            }
            this.visitExpr(((ArrayCall)lvalue).getIndex()); // visit index expression to put it on the stack
            this.visitExpr(assign.getrValue()); // visit rvalue to put it on the stack
            currpw.println("iastore\n"); // store it in the array
        }
    }
}
